<?php
$page = "P1 Traffic Machine";
include 'include/header.php'
?>

<!-- Product Intro Section -->
<section class="padded__section dark product-intro__section">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <div class="row">
            <div class="col-sm-12">
                <h1 class="section__title">P1 Traffic Machine</h1>
                <h4 class="section__subtitle">
                    The web’s only fully automated, SEO-Silo building wordpress plugin
                </h4>
            </div>
        </div>

        <p>
            Traffic is the lifeblood of every website. Without traffic, you cannot create a tribe of followers, build an email list of subscribers or sell your products and services to customers.
            While some people chose to gamble with PPC or spin the roulette with other paid traffic schemes, sites that are built with great SEO standards and rank well in the search engine results can get 10,000s of visitors free every day.
        </p>

        <p>
            The challenge with SEO is that it’s a lot of work! Keyword research, building the right silo structure on your sites, creating hundreds of pages of content, mixing in video, infographics, images and social media...the list is endless and it can take forever…
        </p>

        <p>
            Until now! P1 Traffic Machine is your solution to the SEO problem.
        </p>

        <!-- Product Intro Slider -->
        <section class="product-intro__slider">

            <!-- Slide 1 -->
            <section class="slider__slide first">
                <img src="img/products/p1tm/slide1.jpg" alt="">
            </section>

            <!-- Slide 2 -->
            <section class="slider__slide next">
                <img src="img/products/p1tm/slide2.jpg" alt="">
            </section>

            <!-- Slide 3 -->
            <section class="slider__slide last">
                <img src="img/products/p1tm/slide3.jpg" alt="">
            </section>

        </section>

    </div>
</section>

<!-- Features Section -->
<section class="padded__section">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <h1 class="section__title">Primary Features</h1>
        <p class="section__paragraph">
            P1 Traffic Machine was built from the ground up to solve each one of the big SEO challenges. It works automatically, implementing SEO Best Practices, adding in multiple revenue streams, and curating keyword rich content. This means you don’t need to slave away at the keyboard for hours on end or spend a fortune hiring and trying to manage a team of offshore VAs.
        </p>

        <!-- Reasons List -->
        <div class="columns__section">

            <div class="row">
                <section class="columns__item col-sm-4">
                    <i class="fa fa-th-large"></i>
                    <h4>Silo Website Generator</h4>
                    <p>Automatically create a properly SEO optimized, Silo structured and content ready website in virtually any niche. In minutes!</p>
                </section>

                <section class="columns__item col-sm-4">
                    <i class="fa fa-search"></i>
                    <h4>Keyword Generator</h4>
                    <p>Enter your core keyword and generate the highest searched, relevant keywords, prepping your site for the right content.</p>
                </section>

                <section class="columns__item col-sm-4">
                    <i class="fa fa-puzzle-piece"></i>
                    <h4>Layout Manager</h4>
                    <p>Drag and drop simple, you can configure your site in a few mouse clicks. Create a great looking, Google friendly and content rich user experience.</p>
                </section>
            </div>

            <div class="row">
                <section class="columns__item col-sm-4">
                    <i class="fa fa-dollar"></i>
                    <h4>Monetization Curator</h4>
                    <p>This is groundbreaking: Curate, mix and blend 5 different ways to monetize your site on each and every page. Automatically! </p>
                </section>

                <section class="columns__item col-sm-4">
                    <i class="fa fa-edit"></i>
                    <h4>Content Curation</h4>
                    <p>The key to hassle free SEO and great search engine rankings is fresh, relevant content. So this is curated for you on autopilot every single day.</p>
                </section>

                <section class="columns__item col-sm-4">
                    <i class="fa fa-sitemap"></i>
                    <h4>P1 Site Manager</h4>
                    <p>Manage multiple sites? Want to do SEO for client projects? Now you can, without the admin headache. Control all your sites from a single page.</p>
                </section>
            </div>

        </div>

        <!-- Additional Features Section -->
        <section class="padded__section">
            <!-- Section title and Sub title -->
            <h1 class="section__title">Additional Features</h1>

            <!-- Reasons List -->
            <div class="table__section">

                <div class="row">

                    <ul class="col-md-5 col-md-offset-1">
                        <li>
                            <strong>Fast results...</strong> it takes 2 minutes or less to translate an idea to a properly <strong>SEO optimized, Silo structured and Google traffic ready website</strong> - and you even don't need to know anything about SEO!.
                        </li>
                        <li>
                            <strong>Explore new PROFIT markets in minutes...</strong> with P1 Traffic Machine you can launch a brand new money site in any niche in minutes and <strong>start to make money right away.</strong>
                        </li>
                        <li>
                            <strong>Tap into the ‘hidden’ traffic jams in your niche...</strong> click a button and P1 Traffic Machine will discover the latest high-traffic keywords for you, allowing you to expand your site and increase your profits.
                        </li>
                    </ul>

                    <ul class="col-md-5 col-md-offset-1">
                        <li>
                            <strong>Eliminate your outsourcing costs...</strong> if you’re constantly frustrated trying to manage outsourced writers or SE Asia VAs, P1 Traffic Machine can replace much (if not all) of your outsourcing team. Less headaches, less cost and more profit for you.
                        </li>

                        <li>
                            <strong>Scale your website in seconds...</strong> everybody knows this – there is a direct connection between how often you publish and how much traffic you get. You can now publish fresh content every day, on near full autopilot.
                        </li>

                        <li>
                            <strong>Stop The guesswork...</strong> SEO is complicated, it can be hard to learn and even harder to implement everything. P1 Traffic Machine is built on industry leading best practices. No more guesswork, no more rip-off SEO courses, just a tool that works for you every day.
                        </li>
                    </ul>



                </div>

            </div>
        </section>

    </div>

</section>

<?php include 'include/footer.php' ?>