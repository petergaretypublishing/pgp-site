<?php
$page = "Support";
include 'include/header.php'
?>

<!-- Copy Section -->
<section class="padded__section copy">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <h1 class="section__title">Support</h1>
        <h4 class="section__subtitle">
            Welcome to the Peter Garety Support portal.
        </h4>

        <article class="copy__content">
            <p>
                Your satisfaction is our TOP priority and we’re standing by to assist you. If you’re stuck on a
                technical issue, or if you have a question relating to a product you’ve purchased, the fastest way
                to get help is to open a ticket.
            </p>

            <p>
                You’ll get a real answer as quickly as possible. During the week we guarantee a response within
                24 hrs (though often it’s just a couple of hours) and at weekends it can take up to 48hrs.
            </p>

            <p>
                Behind the scenes, we’re busy creating and updating an extensive Knowledge Base, answering
                the most common questions and helping you get the most out of your product purchase.
            </p>
        </article>

    </div>
</section>

<?php include 'include/footer.php' ?>