<?php
$page = "About us";
include 'include/header.php'
?>

<!-- Copy Section -->
<section class="padded__section copy">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <h1 class="section__title">Who are PGP?</h1>

        <article class="copy__content">

            <p>
                We’re a focused, highly skilled team of product creators, marketing specialists and developers.
            </p>

            <p>
                Ask any of our team what they’d miss most if they left PGP and they’ll all tell you its the rest of the team. (that, and the sarcasm, the swearing, the animated chat channel, the ability to make mistakes, the chance to grow and a love of cats of course...)
            </p>

            <p>
                Our founders were both established, successful, 7 Figure entrepreneurs before they joined forces to create Peter Garety Publishing.
            </p>

            <p>
                We don't have a fancy mission statement, a marble floored corporate office or gilt-edged business cards...we have something much more powerful.
            </p>

            <p>
                We have purpose.
            </p>

            <p>
                We develop rockstar products that transform our customer's businesses. We work fast and smart. We don’t sit around in marketing meetings talking about ideas, we ship products that work.
            </p>

            <p>
                We've built an amazing team and have established a track record of successful product launches.
            </p>

            <p>
                If you have a marketing problem, we have an elegant and effective technical solution. If you've got skills but want more, <a href="careers.php">click through</a> to our Careers page.
            </p>

            <h2 class="copy__title">Our Founders</h2>

            <div class="row">
                <div class="col-md-5">
                    <h4>Peter Garety</h4>
                    <p>
                        Peter is founder and CEO of Peter Garety Publishing LLC - a software development company that he started from home in 2011, and that has now grown to nearly 50 full time team members and over 40,000 customers.
                    </p>

                    <p>
                        Peter has been an entrepreneur since he was 13 years old and his main skill set is a combination of strategic vision and marketing.
                    </p>

                    <p>
                        When asked about the single most important ingredient of PGP success, his usual answer is: <br>
                        "Putting the customer’s needs first and taking care of the team members."
                    </p>

                    <p>
                        Peter believes in leading from the front, and he is constantly looking at new ways to drive the company forward, create better products and make a bigger impact in the market place.
                    </p>

                </div>
                <div class="col-md-5 col-md-offset-1">
                    <h4>Andy Fletcher</h4>
                    <p>
                        Andy is a serial entrepreneur on his second ride on the merry-go-round. He holds a Masters Degree in Computer Science from Bristol and (not that you'd know it to look at him) is a third degree black belt martial artist.
                    </p>

                    <p>
                        His first company, DigiResults.com now runs pretty much on autopilot giving him freedom to do whatever he wants. Which, being a workaholic, is building another business.
                    </p>

                    <p>
                        Andy brings a wealth of technical and operational knowledge to the company as well as a slightly unhealthy amount of anal retentive control freakery.
                    </p>

                    <p>
                        His flair for developing systems and processes means the team can focus on getting their work done with minimal interruptions and oversight.
                    </p>
                </div>
            </div>


        </article>

    </div>
</section>

<?php include 'include/footer.php' ?>