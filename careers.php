<?php
$page = "Careers";
include 'include/header.php'
?>

<!-- Careers Intro Section -->
<section class="padded__section intro dark">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <h1 class="section__title">Join us and be a part of something different</h1>
        <h4 class="section__subtitle">
            See a position you'd be perfect for? Apply today
        </h4>

        <input type="button" class="btn btn-primary btn-lg" id="available-positions" value="View Available Positions">

    </div>
</section>

<!-- How we work Section -->
<div class="images__section">
    <section class="image__row">
        <div class="image__portion image1"></div>
        <div class="text__portion">
            <h1 class="section__title">
                Why Work For Us?
            </h1>
            <h4 class="section__subtitle bold">
                “This ain’t your parent’s career path...”
            </h4>
            <p>Gone are the days of clocking to a monolithic corporation, aspiring to one-day breakout of the cubicle drone-force or break into a Dilbert cartoon. </p>

            <p>Today, you can choose a different path.</p>

            <p>Companies like PGP are breaking the mold and setting new standards, both in how we work and how we serve our customer base.</p>

            <p>Lots of companies talk about running Agile development practices and teams - but for many it’s just that. Talk.</p>

            <p>At PGP, things are different. It’s not just our code base that’s Agile; the entire company is. </p>

            <p>We hire based on what you can do and who you are as an individual, not what institutions grace their names on your certificates and diplomas. If you’re good, you’ll go far. If you’re great, you’ll go further.</p>
        </div>
    </section>

    <!-- How we work Section -->
    <section class="image__row">
        <div class="text__portion">
            <h1 class="section__title">
                A Different Way Of Working
            </h1>

            <p>Some companies talk about remote working or telecommuting. We are geographically agnostic. </p>

            <p>You can work from any location around the world, and we currently have team members across the USA, the UK and Europe, South Africa and SE Asia.</p>

            <p>So, if you’re looking to join a hard working, fast moving, team, and if independence and self-determination are important to you, then read on.</p>

            <p>However, if developing your career means nothing more than adding a few "name" companies your CV then you're definitely not who we’re looking for.</p>

        </div>
        <div class="image__portion image2"></div>
    </section>
</div>

<!-- Available Positions Section -->
<section class="padded__section positions__section">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <h2 class="section__title">Available Positions</h2>

        <!-- Jobs Fields -->

        <section class="row jobs__section">

            <!-- Engineering Jobs -->
            <section class="col-md-4 job__field">
                <h1 class="field__title">Engineering</h1>
                <ul class="jobs__list">
                 <li>
                    <span class="job__unavailable">No Jobs Available</span>
                </li>
            </ul>
        </section>

        <!-- Design and UX Jobs -->
        <section class="col-md-4 job__field">
            <h1 class="field__title">Design and UX</h1>
            <ul class="jobs__list">
               <li>
                <span class="job__unavailable">No Jobs Available</span>
            </li>
        </ul>
    </section>

    <!-- Marketing Jobs -->
    <section class="col-md-4 job__field">
        <h1 class="field__title">Marketing</h1>
        <ul class="jobs__list">
            <li>
                <span class="job__unavailable">No Jobs Available</span>
            </li>
        </ul>
    </section>


</section>

</div>
</section>

<?php include 'include/footer.php' ?>