<?php
$page = "careers";
include 'include/header.php'
?>

    <!-- Careers Intro Section -->
    <section class="padded__section intro dark">
        <div class="container-fluid">

            <!-- Section title and Sub title -->
            <h1 class="section__title">Join us and be a part of something different</h1>
            <h4 class="section__subtitle">
                See a position you'd be perfect for? Apply today
            </h4>

            <input type="button" class="btn btn-primary btn-lg" id="available-positions" value="View Available Positions">

        </div>
    </section>

    <!-- How we work Section -->
    <div class="images__section">
        <section class="image__row">
            <div class="image__portion image1"></div>
            <div class="text__portion">
                <h1 class="section__title">
                    How we work
                </h1>
                <h4 class="section__subtitle">
                    We want to build a work environment where people are happy,
                    productive, and interact well.
                </h4>
                <p>
                    Personal interaction in particular is something we emphasize a lot. We work hard to find people who make others want to be around them. We know we're doing a good job of hiring so long as we see people continuing to join simply to work with those who are already here.
                </p>
                <p>
                    We're quite transparent internally. This helps everyone make better local decisions and avoid split-brain behavior. (Plus, we hire curious people, so
                    they generally want to know the details of what's going on.)
                </p>
            </div>
        </section>

        <!-- How we work Section -->
        <section class="image__row">
            <div class="text__portion">
                <h1 class="section__title">
                    Benifits
                </h1>
                <h4 class="section__subtitle">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
                    euismod bibendum laoreet.
                </h4>

                <ul>
                    <li>Health, dental, and vision benefits for you and your family</li>
                    <li>Life insurance and disability benefits</li>
                    <li>Paid parental leave</li>
                    <li>Relocation support</li>
                    <li>401(k) plan</li>
                    <li>Flexible work hours</li>
                    <li>Open vacation policy</li>
                    <li>Gym membership and laundry service</li>
                    <li>Breakfast, lunch, and dinner</li>
                </ul>
            </div>
            <div class="image__portion image2"></div>
        </section>
    </div>

    <!-- Available Positions Section -->
    <section class="padded__section positions__section">
        <div class="container-fluid">

            <!-- Section title and Sub title -->
            <h1 class="section__title">Available Positions</h1>
            <h4 class="section__subtitle">
                We're hiring for a number of different positions around the world.
            </h4>

            <!-- Jobs Fields -->

            <section class="row jobs__section">

                <!-- Engineering Jobs -->
                <section class="col-md-4 job__field">
                    <h1 class="field__title">Engineering</h1>
                    <ul class="jobs__list">
                        <li>
                            <a href="#" class="job__position">Front End Developer</a>
                            <span class="job__location">Toronto, ON</span>
                        </li>
                        <li>
                            <a href="#" class="job__position">Marketing Developer</a>
                            <span class="job__location">Toronto, ON</span>
                        </li>
                        <li>
                            <a href="#" class="job__position">Software Developer</a>
                            <span class="job__location">Montreal, QC</span>
                        </li>
                    </ul>
                </section>

                <!-- Design and UX Jobs -->
                <section class="col-md-4 job__field">
                    <h1 class="field__title">Design and UX</h1>
                    <ul class="jobs__list">
                        <li>
                            <a href="#" class="job__position">Front End Designer</a>
                            <span class="job__location">Toronto, ON</span>
                        </li>
                        <li>
                            <a href="#" class="job__position">UI Designer</a>
                            <span class="job__location">Toronto, ON</span>
                        </li>
                        <li>
                            <a href="#" class="job__position">UX Designer</a>
                            <span class="job__location">Montreal, QC</span>
                        </li>
                        <li>
                            <a href="#" class="job__position">Interaction Designer</a>
                            <span class="job__location">Toronto, ON</span>
                        </li>
                    </ul>
                </section>

                <!-- Marketing Jobs -->
                <section class="col-md-4 job__field">
                    <h1 class="field__title">Marketing</h1>
                    <ul class="jobs__list">
                        <li>
                            <span class="job__unavailable">No Jobs Available</span>
                        </li>
                    </ul>
                </section>


            </section>

        </div>
    </section>

<?php include 'include/footer.php' ?>