<?php
$page = "products";
include 'include/header.php'
?>

    <!-- Product Intro Section -->
    <section class="padded__section dark product-intro__section">
        <div class="container-fluid">

            <!-- Section title and Sub title -->
            <div class="row">
                <div class="col-sm-9">
                    <h1 class="section__title">P1 Video Magnet</h1>
                    <h4 class="section__subtitle">
                        A Responsive Video Sharing Wordpress Theme
                    </h4>
                </div>
                <div class="col-sm-3">
                    <a href="http://asaadmahmood.com/asaad/videotheme/" target="_blank"><input type="button" class="btn btn-default btn-lg btn-demo" value="Live Demo"></a>
                </div>
            </div>

            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
            </p>

            <!-- Product Intro Slider -->
            <section class="product-intro__slider">

                <!-- Slide 1 -->
                <section class="slider__slide first">
                    <img src="img/products/pvm/slide1.jpg" alt="">
                </section>

                <!-- Slide 2 -->
                <section class="slider__slide next">
                    <img src="img/products/pvm/slide2.jpg" alt="">
                </section>

                <!-- Slide 3 -->
                <section class="slider__slide last">
                    <img src="img/products/pvm/slide3.jpg" alt="">
                </section>

            </section>

        </div>
    </section>

    <!-- Features Section -->
    <section class="padded__section">
        <div class="container-fluid">

            <!-- Section title and Sub title -->
            <h1 class="section__title">Primary Features</h1>
            <h4 class="section__subtitle">
                Here are some incredible features this theme supports.
            </h4>

            <!-- Reasons List -->
            <div class="columns__section">

                <div class="row">
                    <section class="columns__item col-sm-4">
                        <i class="fa fa-mobile"></i>
                        <h4>Fully Responsive</h4>
                        <p>The theme layout adapts to different screen sizes and makes sure your site look good on all devices either mobile or desktop.</p>
                    </section>

                    <section class="columns__item col-sm-4">
                        <i class="fa fa-eye"></i>
                        <h4>Retina Ready</h4>
                        <p>PVM is fully optimized for a stunning look on high resolution – Retina displays as on Apple’s latest products.</p>
                    </section>

                    <section class="columns__item col-sm-4">
                        <i class="fa fa-edit"></i>
                        <h4>Front-end post/edit</h4>
                        <p>You can instantly publish or edit a post right from the front-end using the front-end post form &amp; front-end editor.</p>
                    </section>
                </div>

                <div class="row">
                    <section class="columns__item col-sm-4">
                        <i class="fa fa-photo"></i>
                        <h4>Various Homepage Sliders</h4>
                        <p>We provide you with 3 different sliders for the homepage for you to choose from. Each slider is customizable based on your needs.</p>
                    </section>

                    <section class="columns__item col-sm-4">
                        <i class="fa fa-share-alt"></i>
                        <h4>Social Sharing Options</h4>
                        <p>We provide beautiful social sharing options for your users to be engaged with the post and share it with their friends.</p>
                    </section>

                    <section class="columns__item col-sm-4">
                        <i class="fa fa-cogs"></i>
                        <h4>Various Widgets</h4>
                        <p>PVM provides you with beautiful widgets that are customizable and can be placed inside the sidebar or the footer.</p>
                    </section>
                </div>

            </div>

            <!-- Additional Features Section -->
            <section class="padded__section">
                <!-- Section title and Sub title -->
                <h1 class="section__title">Additional Features</h1>
                <h4 class="section__subtitle">
                    Here are some incredible features this theme supports.
                </h4>

                <!-- Reasons List -->
                <div class="table__section">

                    <div class="row">
                        <section class="col-sm-4">

                            <div class="col-sm-12 table__item">
                                <i class="fa fa-check"></i><span>Customizer Options</span>
                            </div>
                            <div class="col-sm-12 table__item">
                                <i class="fa fa-check"></i><span>Portfolio Template</span>
                            </div>
                            <div class="col-sm-12 table__item">
                                <i class="fa fa-check"></i><span>Custom Header</span>
                            </div>
                            <div class="col-sm-12 table__item">
                                <i class="fa fa-check"></i><span>Featured Images</span>
                            </div>

                        </section>

                        <section class="col-sm-4">

                            <div class="col-sm-12 table__item">
                                <i class="fa fa-check"></i><span>Responsive Framework</span>
                            </div>
                            <div class="col-sm-12 table__item">
                                <i class="fa fa-check"></i><span>Google Fonts</span>
                            </div>
                            <div class="col-sm-12 table__item">
                                <i class="fa fa-check"></i><span>Custom Slider</span>
                            </div>
                            <div class="col-sm-12 table__item">
                                <i class="fa fa-check"></i><span>Featured Images</span>
                            </div>

                        </section>

                        <section class="col-sm-4">

                            <div class="col-sm-12 table__item">
                                <i class="fa fa-check"></i><span>Shortcodes</span>
                            </div>
                            <div class="col-sm-12 table__item">
                                <i class="fa fa-check"></i><span>Html5 and Css3</span>
                            </div>
                            <div class="col-sm-12 table__item">
                                <i class="fa fa-check"></i><span>Custom Footer</span>
                            </div>
                            <div class="col-sm-12 table__item">
                                <i class="fa fa-check"></i><span>Multiple Widgets</span>
                            </div>

                        </section>

                    </div>

                </div>
            </section>

        </div>

    </section>


    <!-- Product Images Section -->
    <section class="product-image__section">
        <div class="col-xs-4">
            <img src="img/products/pvm/layout1.jpg" alt="">
        </div>
        <div class="col-xs-4">
            <img src="img/products/pvm/layout2.jpg" alt="">
        </div>
        <div class="col-xs-4">
            <img src="img/products/pvm/layout3.jpg" alt="">
        </div>
    </section>


    <!-- Products Section -->
    <section class="padded__section">
        <div class="container-fluid">

            <!-- Section title and Sub title -->
            <h1 class="section__title">Our latest work</h1>
            <h4 class="section__subtitle">
                Have a look at some of the products we deliver, we love what we do
                and people love us for it.
            </h4>

            <!-- Products List -->
            <div class="products__list">
                <div class="row">

                    <!-- PVM -->
                    <a href="pvm.html">
                        <section class="product__item col-sm-4 mix">
                            <figure>
                                <img src="img/products/pvm/icon.jpg" height="175" width="175" alt="">
                            </figure>
                            <footer>
                                <h3>P1 Video Magnet</h3>
                                <h5>Responsive Video Sharing Theme</h5>
                            </footer>
                        </section>
                    </a>

                    <!-- PTM -->
                    <a href="pvm.html">
                        <section class="product__item col-sm-4 mix">
                            <figure>
                                <img src="img/products/ptm.jpg" height="175" width="175" alt="">
                            </figure>
                            <footer>
                                <h3>P1 Traffic Machine</h3>
                                <h5>Data driven Traffic Generation Plugin</h5>
                            </footer>
                        </section>
                    </a>

                    <!-- P1MC -->
                    <a href="pvm.html">
                        <section class="product__item col-sm-4 mix">
                            <figure>
                                <img src="img/products/p1mc.jpg" height="175" width="175" alt="">
                            </figure>
                            <footer>
                                <h3>P1 Media Creator</h3>
                                <h5>Facebook OG Images Creator</h5>
                            </footer>
                        </section>
                    </a>

                </div>

            </div>

        </div>
    </section>

<?php include 'include/footer.php' ?>