<?php
$page = "home";
include 'include/header.php'
?>

<!-- Intro Section -->
<section class="padded__section dark intro home__section text-center">
    <div class="container-fluid">
        <h1 class="section__title">
            "Plug and Play Marketing Solutions For Your Online Business"
        </h1>
        <h4 class="section__subtitle">
            Trusted by over 40,000 customers, and solving marketing problems on hundreds of thousands<br>
            of sites, our products can help you grow your business.
        </h4>
        <input type="button" class="btn btn-primary btn-lg" id="browse-products" value="Browse Products">
    </div>
</section>


<!-- Homepage Slider -->
<section class="padded__section" id="homepage-slider">

    <!-- Slider 1 -->
    <section class="slider__slide">
        <div class="row">
            <figure class="slider__image col-sm-7">
                <img src="img/slides/slide1.png" alt="">
            </figure>
            <article class="slider__text col-sm-5">
                <i class="fa fa-mobile"></i>
                <h1>Resvponsive</h1>
                <h3>Compatible with all devices</h3>
                <p>
                    The majority of our themes are created with a fluid,
                    responsive grid. That means your site will display
                    beautifully on any device.
                </p>
            </article>
        </div>
    </section>

    <!-- Slider 2 -->
    <section class="slider__slide">
        <div class="row">
            <figure class="slider__image col-sm-7">
                <img src="img/slides/slide2.png" alt="">
            </figure>
            <article class="slider__text col-sm-5">
                <i class="fa fa-code"></i>
                <h1>Clean Code</h1>
                <h3>Clean and Commented Code</h3>
                <p>
                    We write clean and well commented code. That makes it
                    easy to understand and
                    accessible for you if you want to dive in and add your own
                    functions.
                </p>
            </article>
        </div>
    </section>

    <!-- Slider 3 -->
    <section class="slider__slide">
        <div class="row">
            <figure class="slider__image col-sm-7">
                <img src="img/slides/slide3.png" alt="">
            </figure>
            <article class="slider__text col-sm-5">
                <i class="fa fa-gear fa-spin"></i>
                <h1>Customizable</h1>
                <h3>Extremely customizable code</h3>
                <p>
                    We believe less can be more. We don't overwhelm users
                    with endless, confusing options. Instead, you get everything you need to customize
                    the theme, and nothing you don't.
                </p>
            </article>
        </div>
    </section>

    <!-- Slider 4 -->
    <section class="slider__slide">
        <div class="row">
            <figure class="slider__image col-sm-7">
                <img src="img/slides/slide4.png" alt="">
            </figure>
            <article class="slider__text col-sm-5">
                <i class="fa fa-html5"></i>
                <h1>Technology</h1>
                <h3>Latest Technological Resources</h3>
                <p>
                    We make use of the latest developments to build our
                    solutions. This means you get advanced, stable and highly efficient themes and
                    plugins to extend the capabilities of your site.
                </p>
            </article>
        </div>
    </section>

    <!-- Slider 5 -->
    <section class="slider__slide">
        <div class="row">
            <figure class="slider__image col-sm-7">
                <img src="img/slides/slide5.png" alt="">
            </figure>
            <article class="slider__text col-sm-5">
                <i class="fa fa-puzzle-piece"></i>
                <h1>Compatible</h1>
                <h3>Browser Compatible</h3>
                <p>
                    Your site visitors can be coming from anywhere in the world,
                    on almost any platform so we make sure our products have the widest cross browser
                    compatibility possible.
                </p>
            </article>
        </div>
    </section>

</section>

<!-- Products Section -->
<section class="padded__section bordered latest-work__section">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <h1 class="section__title">Our latest work</h1>
        <h4 class="section__subtitle">
            Have a look at some of the products we deliver, we love what we do
            and people love us for it.
        </h4>

        <!-- Products List -->
        <div class="products__list">
            <div class="row">

                <!-- PVM -->
                <a href="pvm.php">
                    <section class="product__item col-sm-4 mix">
                        <figure>
                            <img src="img/products/pvm/icon.jpg" height="175" width="175" alt="">
                        </figure>
                        <footer>
                            <h3>P1 Video Magnet</h3>
                            <h5>Responsive Video Sharing Theme</h5>
                        </footer>
                    </section>
                </a>

                <!-- PTM -->
                <a href="p1tm.php">
                    <section class="product__item col-sm-4 mix">
                        <figure>
                            <img src="img/products/p1tm/icon.jpg" height="175" width="175" alt="">
                        </figure>
                        <footer>
                            <h3>P1 Traffic Machine</h3>
                            <h5>Data driven Traffic Generation Plugin</h5>
                        </footer>
                    </section>
                </a>

                <!-- P1MC -->
                <a href="pvm.php">
                    <section class="product__item col-sm-4 mix">
                        <figure>
                            <img src="img/products/p1mc.jpg" height="175" width="175" alt="">
                        </figure>
                        <footer>
                            <h3>P1 Media Creator</h3>
                            <h5>Facebook OG Images Creator</h5>
                        </footer>
                    </section>
                </a>

            </div>

        </div>

        <!-- Button -->
        <a href="products.php"><input type="button" class="btn btn-default btn-lg" value="View all Products"></a>

    </div>
</section>

<!-- Why Choose us Section -->
<section class="padded__section bordered">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <h1 class="section__title">Why choose us?</h1>
        <h4 class="section__subtitle">
            Here are some of the reasons why you should choose us and be one of our treasured customer.
        </h4>

        <!-- Reasons List -->
        <div class="columns__section">

            <div class="row">
                <section class="columns__item col-sm-4">
                    <i class="fa fa-life-ring"></i>
                    <h4>Small &amp; Dedicated Team</h4>
                    <p>We are a highly focused team who live and breathe Wordpress, SEO and Online Marketing. We’re committed to creating easy to use products to help you grow your online business.</p>
                </section>

                <section class="columns__item col-sm-4">
                    <i class="fa fa-search"></i>
                    <h4>SEO Friendly</h4>
                    <p>Our core plugins are designed to be at the forefront of the latest SEO best practices and are rigorously tested on thousands of niche WP sites before launch.</p>
                </section>

                <section class="columns__item col-sm-4">
                    <i class="fa fa-play-circle-o"></i>
                    <h4>Product Tutorials</h4>
                    <p>Each product comes with a full suite of tutorials, so you don’t have to waste time figuring out how to use (and how to profit from) our products.</p>
                </section>
            </div>

            <div class="row">

                <section class="columns__item col-sm-4">
                    <i class="fa fa-user"></i>
                    <h4>User Centric Design</h4>
                    <p>Our products are designed around your needs and the most efficient workflows. Intuitive, easy to use interfaces come as standard.</p>
                </section>

                <section class="columns__item col-sm-4">
                    <i class="fa fa-level-up"></i>
                    <h4>Frequent Updates</h4>
                    <p>We’re constantly updating and improving our product suite. Our update process is seamless and automatic, one click and you’re rocking all the latest features, bug fixes and upgrades.</p>
                </section>

                <section class="columns__item col-sm-4">
                    <i class="fa fa-wordpress"></i>
                    <h4>Wordpress Specific</h4>
                    <p>We’re committed to Wordpress and develop exclusively for this platform. All our products are tested extensively against each new Wordpress release to ensure maximum compatibility.</p>
                </section>

            </div>

        </div>


    </div>
</section>

<!-- Testimonials Section -->
<section class="padded__section dark">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <h1 class="section__title">What people say</h1>
        <h4 class="section__subtitle">
            Here is some of the feedback we’ve received over the years from our valued customers.
        </h4>

        <div class="testimonials__section">
            <div class="icon">
                <i class="fa fa-quote-left"></i>
            </div>

            <!-- Testimonial Slider -->
            <div class="testimonials__container" id="testimonial-slider">

                <!-- Testimonial Item -->
                <div class="slider__slide">
                    <article class="testimonial__box">
                        <p>
                            Awesome job and thank you for your help! I am AMAZED at the level of support. Hats of to Peter for an exceptional product and the best product support I've ever dealt with.
                        </p>
                    </article>

                    <div class="testimonial__info">
                        <span class="testimonial__name">Bob</span>
                        <span class="testimonial__product">@ P1 Video Magnet</span>
                    </div>
                </div>

                <!-- Testimonial Item -->
                <div class="slider__slide">
                    <article class="testimonial__box">
                        <p>
                            I want to say how awesome this software is. The keyword planner saves so much time and hastle! Awesome that you can just upload a complete list from key work planner!.
                        </p>
                    </article>

                    <div class="testimonial__info">
                        <span class="testimonial__name">Irene</span>
                        <span class="testimonial__product">@ Dashnex Plguin</span>
                    </div>
                </div>

                <!-- Testimonial Item -->
                <div class="slider__slide">
                    <article class="testimonial__box">
                        <p>
                            I deal with quite a few "Support Desks" with all kids of results and PeterGarety Support Team really is at the top of my list.
                        </p>
                    </article>

                    <div class="testimonial__info">
                        <span class="testimonial__name">Jack Deese</span>
                        <span class="testimonial__product">@ Support Team</span>
                    </div>
                </div>

            </div>
            <div class="icon">
                <i class="fa fa-quote-right"></i>
            </div>
        </div>

    </div>
</section>

<!-- Subscribe Section -->
<section class="padded__section subscribe__section">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-8">

                <!-- Section title and Sub title -->
                <h1 class="section__title">Subscribe and get regular updates!</h1>
                <h4 class="section__subtitle">
                    Subscribe to our newsletter to receive important news regarding theme updates, new products, promotions, freebies and more from PeterGaretyPublishing.
                </h4>
                <div class="subscribe__form">
                    <div class="cell subscribe__textfield">
                        <input type="text" class="form-control form-group" placeholder="Enter your email">
                    </div>
                    <div class="cell subscribe__button">
                        <input type="button" class="btn btn-default btn-lg" value="Submit">
                    </div>
                </div>
            </div>

            <div class="col-md-4 subscribe__icon"><i class="fa fa-4x fa-envelope"></i></div>

        </div>

    </div>
</section>

<?php include 'include/footer.php' ?>