<?php
$page = "about";
include 'include/header.php'
?>

<!-- About Intro Section -->
<section class="padded__section intro dark">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <h1 class="section__title">Awesome people creating awesome products</h1>
        <h4 class="section__subtitle">
            We help our customers achieve they’ve been longing for
        </h4>

    </div>
</section>

<!-- Copy Section -->
<section class="padded__section copy">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <h1 class="section__title">About us</h1>
        <h4 class="section__subtitle">Get to know about us and our team.</h4>

        <article class="copy__content">
            <p>
                PeterGateyPublishing was founded in 2009 by Peter Garety in the town of Lahaina on the Hawaiian Island of Maui.
            </p>

            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis odio vitae dictum efficitur. Nunc volutpat odio eu quam semper, ut congue odio aliquam. Nulla laoreet lobortis libero, vel euismod justo feugiat nec. Integer elementum tempor quam, eu consectetur lacus facilisis non. Sed risus quam, dignissim id
                pretium vel, efficitur id velit. Proin semper neque eget justo rhoncus porta. Quisque ut nibh sed nisi placerat tincidunt vitae id neque. Quisque nibh purus, venenatis non condimentum nec, scelerisque id nibh. Nulla rutrum mollis dui, eget vehicula arcu porta condimentum.
            </p>

            <h3 class="copy__title">Who are PGP?</h3>

            <p>
                We’re a focused, highly skilled team of product creators, marketing specialists and developers.
            </p>

            <p>
                Ask any of our team what they’d miss most if they left PGP and they’ll all tell you its the rest of the team. (that, and the sarcasm, the swearing, the animated chat channel, the ability to make mistakes, the chance to grow and a love of cats of course...)
            </p>

            <p>
                Our founders were both established, successful, 7 Figure entrepreneurs before they joined forces to create Peter Garety Publishing.
            </p>

            <p>
                We don't have a fancy mission statement, a marble floored corporate office or gilt-edged business cards...we have something much more powerful.
            </p>

            <p>
                We have purpose.
            </p>

            <p>
                We develop rockstar products that transform our customer's businesses. We work fast and smart. We don’t sit around in marketing meetings talking about ideas, we ship products that work.
            </p>

            <p>
                We've built an amazing team and have established a track record of successful product launches.
            </p>

            <p>
                If you have a marketing problem, we have an elegant and effective technical solution. If you've got skills but want more, <a href="careers.php">click through</a> to our Careers page.
            </p>
        </article>

    </div>
</section>

<?php include 'include/footer.php' ?>