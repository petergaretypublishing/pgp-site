<?php
$page = "Products";
include 'include/header.php'
?>

<!-- Products Section -->
<section class="padded__section copy products__section">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <h1 class="section__title">WordPress Products</h1>
        <h4 class="section__subtitle">
            Our WordPress Themes and Plugins
        </h4>

        <ul class="products__sorting">
            <li class="sort__label">Sort By</li>
            <li class="filter active" data-filter="all">All</li>
            <li class="filter" data-filter="sort__theme">Themes</li>
            <li class="filter" data-filter="sort__plugin">Plugins</li>
            <li class="filter" data-filter="sort__seo">Seo</li>
            <li class="filter" data-filter="sort__marketing">Marketing</li>
            <li class="filter" data-filter="sort__traffic">Traffic Generation</li>
        </ul>

        <!-- Products List -->
        <div class="products__list">
            <div class="row">

                <!-- PVM -->
                <a href="pvm.php">
                    <section class="product__item col-sm-4 mix sort__theme">
                        <figure>
                            <img src="img/products/pvm/icon.jpg" height="175" width="175" alt="">
                        </figure>
                        <footer>
                            <h3>P1 Video Magnet</h3>
                            <h5>Responsive Video Sharing Theme</h5>
                        </footer>
                    </section>
                </a>

                <!-- PTM -->
                <a href="p1tm.php">
                    <section class="product__item col-sm-4 mix sort__plugin sort__traffic">
                        <figure>
                            <img src="img/products/p1tm/icon.jpg" height="175" width="175" alt="">
                        </figure>
                        <footer>
                            <h3>P1 Traffic Machine</h3>
                            <h5>Data driven Traffic Generation Plugin</h5>
                        </footer>
                    </section>
                </a>

                <!-- P1MC -->
                <a href="pvm.php">
                    <section class="product__item col-sm-4 mix sort__plugin sort__marketing">
                        <figure>
                            <img src="img/products/p1mc.jpg" height="175" width="175" alt="">
                        </figure>
                        <footer>
                            <h3>P1 Media Creator</h3>
                            <h5>Facebook OG Images Creator</h5>
                        </footer>
                    </section>
                </a>


                <!-- WPT -->
                <a href="pvm.php">
                    <section class="product__item col-sm-4 mix sort__theme sort__marketing">
                        <figure>
                            <img src="img/products/wpt.jpg" height="175" width="175" alt="">
                        </figure>
                        <footer>
                            <h3>WP Pinboard Theme</h3>
                            <h5>Responsive Image Sharing Theme</h5>
                        </footer>
                    </section>
                </a>

                <!-- WSB -->
                <a href="pvm.php">
                    <section class="product__item col-sm-4 mix sort__plugin sort__marketing">
                        <figure>
                            <img src="img/products/wsb.jpg" height="175" width="175" alt="">
                        </figure>
                        <footer>
                            <h3>WP Squeeze Box</h3>
                            <h5>Teaser and Thank you Page Designer Plugin</h5>
                        </footer>
                    </section>
                </a>

                <!-- WSP -->
                <a href="pvm.php">
                    <section class="product__item col-sm-4 mix sort__plugin sort__traffic">
                        <figure>
                            <img src="img/products/wsp.jpg" height="175" width="175" alt="">
                        </figure>
                        <footer>
                            <h3>WP Sales Pro</h3>
                            <h5>Post sharing Multi Layout Theme</h5>
                        </footer>
                    </section>
                </a>

                <!-- WSN -->
                <a href="pvm.php">
                    <section class="product__item col-sm-4 mix sort__plugin sort__traffic">
                        <figure>
                            <img src="img/products/wsn.jpg" height="175" width="175" alt="">
                        </figure>
                        <footer>
                            <h3>WP Social Navigator</h3>
                            <h5>Social Media Navigation Plugin</h5>
                        </footer>
                    </section>
                </a>

                <!-- PVC -->
                <a href="pvm.php">
                    <section class="product__item col-sm-4 mix sort__plugin sort__marketing">
                        <figure>
                            <img src="img/products/pvc.jpg" height="175" width="175" alt="">
                        </figure>
                        <footer>
                            <h3>P1 Video Curator</h3>
                            <h5>Video Embedder Plugin</h5>
                        </footer>
                    </section>
                </a>

                <!-- PNM -->
                <a href="pvm.php">
                    <section class="product__item col-sm-4 mix sort__plugin sort__seo">
                        <figure>
                            <img src="img/products/pnm.jpg" height="175" width="175" alt="">
                        </figure>
                        <footer>
                            <h3>P1 Niche Magnet</h3>
                            <h5>Social Media Availability Checker Plugin</h5>
                        </footer>
                    </section>
                </a>

            </div>
        </div>

    </div>
</section>

<?php include 'include/footer.php' ?>