<?php
$page = "Disclaimer";
include '../include/header.php'
?>

    <!-- Copy Section -->
    <section class="padded__section copy">
        <div class="container-fluid">

            <!-- Section title and Sub title -->
            <h1 class="section__title">DISCLAIMER</h1>

            <article class="copy__content">
                <p>The information contained in PeterGaretyPublishing.com is for general information purposes only. The information is provided by PeterGaretyPublishing.com and while we endeavour to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to PeterGaretyPublishing.com or the information, products, services, or related graphics contained on PeterGaretyPublishing.com for any purpose. Any reliance you place on such information is therefore strictly at your own risk.</p>

                <p>In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of PeterGaretyPublishing.com.</p>

                <p>Through PeterGaretyPublishing.com you are able to link to other websites which are not under the control of PeterGaretyPublishing.com. We have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them.</p>

                <p>Every effort is made to keep PeterGaretyPublishing.com up and running smoothly. However,PeterGaretyPublishing.com takes no responsibility for, and will not be liable for, PeterGaretyPublishing.com being temporarily unavailable due to technical issues beyond our control.</p>
            </article>

        </div>
    </section>

<?php include '../include/footer.php' ?>