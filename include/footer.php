</main>

<!-- Main Footer -->
<footer class="padded__section dark footer__section">
    <div class="container-fluid">

        <!-- Footer Company Info -->
        <section class="foooter__company col-md-3">

            <!-- Logo -->
            <img src="/img/logo.png" class="header__logo" height="53px" width="88px" alt="">
            <p>
                Copyright © 2011 - 2014 PGP<br>
                All right reserved
            </p>

        </section>

        <!-- Footer Links -->
        <div class="footer__links col-md-9">

            <ul>
                <li class="footer__heading">Services</li>
                <li class="<?php echo ($page == "Home" ? "active" : "")?>"><a href="/">Home</a></li>
                <li class="<?php echo ($page == "Support" ? "active" : "")?>"><a href="https://petergaretysupport.zendesk.com" target="_blank">Support</a></li>
                <li class="<?php echo ($page == "Feature" ? "active" : "")?>"><a href="https://petergaretysupport.zendesk.com" target="_blank">Feature Requests</a></li>
            </ul>

            <ul>
                <li class="footer__heading">Company</li>
                <li class="<?php echo ($page == "About us" ? "active" : "")?>"><a href="/about.php">About us</a></li>
                <li class="<?php echo ($page == "Privacy Policy" ? "active" : "")?>"><a href="/privacy-policy">Privacy Policy</a></li>
                <li class="<?php echo ($page == "Terms of use" ? "active" : "")?>"><a href="/terms-of-use">Terms of use</a></li>
                <li class="<?php echo ($page == "Disclaimer" ? "active" : "")?>"><a href="/disclaimer">Disclaimer</a></li>
            </ul>

            <ul class="social__links">
                <li class="footer__heading">Social</li>
                <li><a href="https://www.facebook.com/petergaretypublishing"><i class="fa fa-facebook"></i></a></li>
                <li><a href="http://twitter.com/petergarety"><i class="fa fa-twitter"></i></a></li>
                <li><a href="http://youtube.com/petergarety"><i class="fa fa-youtube"></i></a></li>
            </ul>

        </div>

    </div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.1.min.js"><\/script>')</script>

<script src="/js/min/plugins-min.js"></script>
<script src="/js/main.js"></script>

</body>
</html>