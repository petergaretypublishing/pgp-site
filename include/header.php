<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo $page ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="icon" type="image/png" href="/favicon.png">

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/css/styles.css">

    <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<main id="wrapper">

    <!-- Main Header -->
    <header class="main-header">
        <div class="container-fluid">

            <nav class="navbar-header">
                <!-- Logo -->
                <a href="/"><img src="/img/logo.png" class="header__logo" height="53px" width="88px" alt=""></a>

                <!-- Mobile Navigation Button -->
                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#header__navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </nav>

            <!-- Navigation -->
            <nav id="header__navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="<?php echo ($page == "Home" ? "active" : "")?>"><a href="/">Home</a></li>
                    <li class="<?php echo ($page == "Support" ? "active" : "")?>"><a href="https://petergaretysupport.zendesk.com" target="_blank">Support</a></li>
                    <li class="<?php echo ($page == "Careers" ? "active" : "")?>"><a href="/careers.php">Careers</a></li>
                    <li class="<?php echo ($page == "About us" ? "active" : "")?>"><a href="/about.php">About us</a></li>
                </ul>
            </nav>

        </div>
    </header>