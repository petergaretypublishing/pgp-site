</main>

<!-- Main Footer -->
<footer class="padded__section dark footer__section">
    <div class="container-fluid">

        <!-- Footer Company Info -->
        <section class="foooter__company col-md-3">

            <!-- Logo -->
            <img src="img/logo.png" class="header__logo" height="53px" width="88px" alt="">
            <p>
                Copyright © 2010 - 2014 PGP<br>
                All right reserved
            </p>

        </section>

        <!-- Footer Links -->
        <div class="footer__links col-md-9">

            <ul>
                <li class="footer__heading">Services</li>
                <li class="<?php echo ($page == "products" ? "active" : "")?>"><a href="products.php">Products</a></li>
                <li class="<?php echo ($page == "wordpress" ? "active" : "")?>"><a href="wordpress.php">Wordpress</a></li>
                <li class="<?php echo ($page == "support" ? "active" : "")?>"><a href="https://petergaretysupport.zendesk.com" target="_blank">Support</a></li>
                <li class="<?php echo ($page == "feature" ? "active" : "")?>"><a href="https://petergaretysupport.zendesk.com" target="_blank">Feature Requests</a></li>
            </ul>

            <ul>
                <li class="footer__heading">Company</li>
                <li class="<?php echo ($page == "about" ? "active" : "")?>"><a href="about.php">About</a></li>
                <li class="<?php echo ($page == "privacy" ? "active" : "")?>"><a href="privacy.php">Privacy Policy</a></li>
                <li class="<?php echo ($page == "terms" ? "active" : "")?>"><a href="terms.php">Terms of use</a></li>
                <li class="<?php echo ($page == "disclaimer" ? "active" : "")?>"><a href="disclaimer.php">Disclaimer</a></li>
            </ul>

            <ul class="social__links">
                <li class="footer__heading">Social</li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            </ul>

        </div>

    </div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')</script>

<script src="js/min/plugins-min.js"></script>
<script src="js/main.js"></script>

</body>
</html>