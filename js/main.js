$(document).ready(function() {

	// Scrolling function
	var scrollToElement = function(el, ms){
		var speed = (ms) ? ms : 600;
		$('html,body').animate({
			scrollTop: $(el).offset().top
		}, speed);
	}

	// Browse Products Button
	$('#browse-products').click(function(){
		scrollToElement('.latest-work__section', 600);
	});


	// View Available Positions Button
	$('#available-positions').click(function(){
		scrollToElement('.positions__section', 600);
	});

	// Homepage Slider
	$("#homepage-slider").owlCarousel({

		dots: false,
		nav: true,
		autoplay : true,
		autoplaySpeed: 800,
		autoplayTimeout:10000,
		navSpeed: 800,
		loop: true,
		dragEndSpeed: 500,
		items:1,
		navText : ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>']

	});

	// Testimonial Slider
	$("#testimonial-slider").owlCarousel({

		animateOut: 'fadeOut',
		dots: true,
		autoplay : true,
		autoplaySpeed: 800,
		autoplayTimeout:10000,
		dotsSpeed: 800,
		loop: true,
		dragEndSpeed: 500,
		items:1

	});

	// Product Intro Slider
	$('body').on('click', '.product-intro__slider .slider__slide', function(){

		var slideWidth = $('.product-intro__slider .slider__slide').outerWidth();
		var totalWidth = $('.product-intro__slider').outerWidth();
		var margin =  totalWidth - slideWidth

		if($(this).hasClass('next')){
			$(this).closest('.product-intro__slider').find('.last').removeClass('last').addClass('next').css('left','0');
			$(this).closest('.product-intro__slider').find('.first').removeClass('first').addClass('last').css('left',margin);
			$(this).removeClass('next').addClass('first').css('left',margin/2);
		}

		else if($(this).hasClass('last')){
			$(this).closest('.product-intro__slider').find('.next').removeClass('next').addClass('last').css('left',margin);
			$(this).closest('.product-intro__slider').find('.first').removeClass('first').addClass('next').css('left','0');
			$(this).removeClass('last').addClass('first').css('left',margin/2);
		}

	});

	// Product Intro Slider First Slide Margin
	var firstSlide = function(){
		var slideWidth = $('.product-intro__slider .slider__slide').outerWidth();
		var slideHeight = $('.product-intro__slider .slider__slide').outerHeight();
		var totalWidth = $('.product-intro__slider').outerWidth();
		var margin =  totalWidth - slideWidth
		$('.product-intro__slider .slider__slide.first').css('left',margin/2)
		$('.product-intro__slider .slider__slide.last').css('left',margin)
		$('.product-intro__slider').css('padding-bottom',slideHeight - 5)
	};

	$(window).load(function(){
		firstSlide();
	});

	// Adjust width on resizse
	$(window).resize(function(){
		firstSlide();
	});


	// Product Page Sorting
	$('.products__list').mixitup();

});
