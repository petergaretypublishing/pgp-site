<?php
$page = "P1 Video Magnet";
include 'include/header.php'
?>

<!-- Product Intro Section -->
<section class="padded__section dark product-intro__section">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <div class="row">
            <div class="col-sm-12">
                <h1 class="section__title">P1 Video Magnet</h1>
                <h4 class="section__subtitle">
                    Build Your Own Multimedia-Powered, SEO Authority Site In Minutes
                </h4>
            </div>
        </div>

        <p>
            You’re probably already aware that Video is the future of the net. More content is uploaded to YouTube each day than can be watched in a 100 lifetimes and video is 20x more likely to rank on the first page of Google than text based content.
        </p>

        <p>
            The bottom line is that mastering Video content is the best way to build authority AND dominate the search engines at the same time. And with P1 Video Magnet, that task just became push-button simple.
        </p>

        <!-- Product Intro Slider -->
        <section class="product-intro__slider">

            <!-- Slide 1 -->
            <section class="slider__slide first">
                <img src="img/products/pvm/slide1.jpg" alt="">
            </section>

            <!-- Slide 2 -->
            <section class="slider__slide next">
                <img src="img/products/pvm/slide2.jpg" alt="">
            </section>

            <!-- Slide 3 -->
            <section class="slider__slide last">
                <img src="img/products/pvm/slide3.jpg" alt="">
            </section>

        </section>

    </div>
</section>

<!-- Features Section -->
<section class="padded__section">
    <div class="container-fluid">

        <!-- Section title and Sub title -->
        <h1 class="section__title">Primary Features</h1>
        <p class="section__paragraph">
            Creating an Authority site using rich multimedia including video solves many of the SEO challenges you’re facing today. Sites with video have a 10x lower bounce rate, which is why google ranks video content higher than plain old regular text articles. Users love Video too. Who has time to read 10,000 words when they can watch a 3min video instead?
        </p>

        <p class="section__paragraph">
            Website owners like you are often daunted by the prospect of building an authority site. When you add in the complex demands of creating a great looking site, with the right structure and multimedia video content, it can be overwhelming.
        </p>

        <p class="section__paragraph">
            P1 Video Magnet solves both the tricky SEO complexities and the difficulty in creating feature-rich, great-looking, video and multimedia authority sites.
        </p>

        <!-- Reasons List -->
        <div class="columns__section">

            <div class="row">
                <section class="columns__item col-sm-4">
                    <i class="fa fa-search"></i>
                    <h4>Keyword Planner</h4>
                    <p>Click Keyword import direct from Google. Import thousands of highly relevant, niche keywords straight into your Wordpress dashboard. </p>
                </section>

                <section class="columns__item col-sm-4">
                    <i class="fa fa-sitemap"></i>
                    <h4>Site Manager</h4>
                    <p>State-of-the-art website development, management and centralised control is now at your finger tips. Build one or a hundred sites, all at the push of a button.</p>
                </section>

                <section class="columns__item col-sm-4">
                    <i class="fa fa-edit"></i>
                    <h4>Content Curator</h4>
                    <p>Automatically find and curate rich keyword relevant content (including high definition video) to create a search engine friendly authority site - in just a few clicks.</p>
                </section>
            </div>

            <div class="row">
                <section class="columns__item col-sm-4">
                    <i class="fa fa-photo"></i>
                    <h4>P1 Video Magnet Theme</h4>
                    <p>Great content demands a great looking theme and the P1 Video Magnet theme is perfect for the job. Responsive, high tech, stable and gorgeous.</p>
                </section>

                <section class="columns__item col-sm-4">
                    <i class="fa fa-paint-brush"></i>
                    <h4>Designed To Convert</h4>
                    <p>We’ve included multiple ways to convert your visitors into additional revenue streams. Manage your advertising, promote products and build your list.</p>
                </section>

                <section class="columns__item col-sm-4">
                    <i class="fa fa-line-chart"></i>
                    <h4>Social Graph Built In!</h4>
                    <p>Social Media and viral content can give your site an exponential traffic boost, so we built in Open Graph technology to turn every share into more visitors for you.</p>
                </section>
            </div>

        </div>

        <!-- Additional Features Section -->
        <section class="padded__section">
            <!-- Section title and Sub title -->
            <h1 class="section__title">Additional Features</h1>
            <h4 class="section__subtitle">
                Here are some incredible features this theme supports.
            </h4>

            <!-- Reasons List -->
            <div class="table__section">

                <div class="row">

                    <ul class="col-md-5 col-md-offset-1">
                        <li>
                            <strong>Facebook Ads generator…</strong> turn any image into a perfectly optimized Facebook Ad so can tap into the power of a billion users and drive even more traffic to your site.
                        </li>
                        <li>
                            <strong>Automatic OG meta activation…</strong> regardless which social button your website visitors will click, P1 Media Creator will generate a custom image that looks good on every single social platform.
                        </li>
                        <li>
                            <strong>Monetization System…</strong> display any type of ads - banner ads, AdSense or even opt-in forms, to the hottest and best converting spots on your site.
                        </li>
                    </ul>

                    <ul class="col-md-5 col-md-offset-1">
                        <li>
                            <strong>MOTION technology…</strong> ”read” the INTENT of your website visitors, engage them to browse more posts/pages on your site or simply get them to opt-in to your email list.
                        </li>

                        <li>
                            <strong>Social Conversion System…</strong> engage your website visitors exactly where they are on your site…get more leads to your email list, subscribers to your YouTube channel or viral traffic from social sites.
                        </li>

                        <li>
                            <strong>Multi-layout Design…</strong> change your home page, post/page or even sidebar look to perfectly match your business needs, whether you're running a small niche website, huge authority site or local business lead generation website.
                        </li>
                    </ul>

                </div>

            </div>
        </section>

    </div>

</section>


<!-- Product Images Section -->
<section class="product-image__section">
    <div class="col-xs-4">
        <img src="img/products/pvm/layout1.jpg" alt="">
    </div>
    <div class="col-xs-4">
        <img src="img/products/pvm/layout2.jpg" alt="">
    </div>
    <div class="col-xs-4">
        <img src="img/products/pvm/layout3.jpg" alt="">
    </div>
</section>


<?php include 'include/footer.php' ?>